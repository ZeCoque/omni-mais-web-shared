export const environment = {
  URL_OMNI_MAIS_API: 'https://omnimais.omni.com.br/api/',
  URL_OMNI_DOC_UPLOAD: 'https://api.omni.com.br/doc-upload/api/',
  URL_OMNI_FACIL: '/omnifacil/',
  URL_OMNI_GERAL: 'https://geral.omni.com.br/api/',
  URL_CDC_LOJA: 'https://prd-omni-cdcl.omni.com.br/api/',
  URL_CREDITO_PESSOAL: 'https://prd-omni-cp.omni.com.br/api/',
  URL_OMNI_VEHICLE_REPORT: 'https://omni-auto-laudo.omni.com.br/api/',
  URL_MICROCREDITO: 'https://prd-omni-cp-mc.omni.com.br/api/',
  URL_LEADS_ONLINE: 'https://prd-omni-leads.omni.com.br/api/',
  production: true,
  VERSION_APP: '2.4.0',
  SHOW_CP: true
};
