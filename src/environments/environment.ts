export const environment = {
  URL_OMNI_MAIS_API: 'https://dev-omnimais.omni.com.br/api/',
  URL_OMNI_DOC_UPLOAD: 'https://dev-api.omni.com.br/doc-upload/api/',
  URL_OMNI_FACIL: '/omnifacil/',
  URL_OMNI_GERAL: 'https://dev-geral.omni.com.br/api/',
  URL_CDC_LOJA: 'https://dev-omni-cdcl.omni.com.br/api/',
  URL_CREDITO_PESSOAL: 'https://dev-omni-cp.omni.com.br/api/',
  URL_MICROCREDITO: 'http://dev-omni-cp-mc.omniaws.local/api/',
  URL_OMNI_VEHICLE_REPORT: 'https://dev-omni-auto-laudo.omni.com.br/api/',
  URL_LEADS_ONLINE: 'https://dev-omni-leads.omni.com.br/api/',
  production: false,
  VERSION_APP: '2.4.0',
  SHOW_CP: true
};
