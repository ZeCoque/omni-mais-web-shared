export const environment = {
  URL_OMNI_MAIS_API: 'https://hmg-omnimais.omni.com.br/api/',
  URL_OMNI_DOC_UPLOAD: 'https://hmg-api.omni.com.br/doc-upload/api/',
  URL_OMNI_FACIL: '/omnifacil/',
  URL_OMNI_GERAL: 'https://hmg-geral.omni.com.br/api/',
  URL_CDC_LOJA: 'https://hmg-omni-cdcl.omni.com.br/api/',
  URL_CREDITO_PESSOAL: 'https://hmg-omni-cp.omni.com.br/api/',
  URL_OMNI_VEHICLE_REPORT: 'https://hmg-omni-auto-laudo.omni.com.br/api/',
  URL_MICROCREDITO: 'https://hmg-omni-cp-mc.omni.com.br/api/',
  URL_LEADS_ONLINE: 'https://hmg-omni-leads.omni.com.br/api/',
  production: false,
  VERSION_APP: '2.4.0',
  SHOW_CP: true
};
