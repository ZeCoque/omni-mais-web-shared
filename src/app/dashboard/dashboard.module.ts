import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BreadcrumbModule } from '../breadcrumb/breadcrumb.module';
import { OmniRestModule } from '../omni-rest/omni-rest.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { MeuDesempenhoModule } from './meu-desempenho/meu-desempenho.module';
import { RankingComponent } from './ranking/ranking.component';
import { RankingModule } from './ranking/ranking.module';

@NgModule({
  imports: [
    CommonModule,
    MeuDesempenhoModule,
    RankingModule,
    OmniRestModule,
    BreadcrumbModule,
  ],
  declarations: [
    DashboardComponent,
    RankingComponent
  ],
  exports: [
    DashboardComponent,
    RankingComponent
  ],
  providers: [
    DashboardService
  ]
})
export class DashboardModule {}
