import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BtnGroupModule } from 'src/app/btn-group/btn-group.module';
import { PanelModule } from './../panel/panel.module';
import { SelectPeriodoComponent } from './select-periodo.component';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    BtnGroupModule
  ],
  declarations: [SelectPeriodoComponent],
  exports: [SelectPeriodoComponent]
})
export class SelectPeriodoModule { }
