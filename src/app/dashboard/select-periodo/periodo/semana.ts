import * as moment from 'moment';
import { Periodo } from './periodo';

export class Semana implements Periodo {

  padrao = false;

  descricao = 'Semana';

  constructor(padrao?: boolean) {
    this.padrao = padrao || false;
  }

  get dataInicio(): number {
    return moment().day('monday').unix() * 1000;
  }

  get dataFim(): number {
    return moment.now();
  }
}
