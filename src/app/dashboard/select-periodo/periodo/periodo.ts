export interface Periodo {
  padrao: boolean;
  descricao: string;
  dataInicio: number;
  dataFim: number;
}
