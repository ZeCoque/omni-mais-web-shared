export interface Agrupamento {
  descricao: string;
  getKey(proposta: any): string;
  compare(proposta1: any, proposta2: any): number;
}
