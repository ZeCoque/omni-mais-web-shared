import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css'],
})
export class RecordComponent implements OnInit, OnChanges {
  @Input() label: string;
  @Input() propostas: any[];
  @Input() periodo;

  isShowList = true;
  constructor() {}

  ngOnInit() {}

  ngOnChanges() {
    this.isShowList = this.periodo !== 'Mês';
  }

  onHideShowRecordsList() {
    this.isShowList = !this.isShowList;
  }
}
