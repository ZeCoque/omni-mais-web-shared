import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PanelModule } from '../../panel/panel.module';
import { PainelRelacaoFichasComponent } from './painel-relacao-fichas.component';
import { RecordListComponent } from './record-list/record-list.component';
import { RecordComponent } from './record/record.component';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    BrowserAnimationsModule,
    BrowserModule,
  ],
  declarations: [
    PainelRelacaoFichasComponent,
    RecordComponent,
    RecordListComponent
  ],
  exports: [PainelRelacaoFichasComponent]
})
export class PainelRelacaoFichasModule { }
