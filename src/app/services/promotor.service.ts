import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OmniGeralRestService } from '../omni-rest/omni-geral/omni-geral-rest.service';

@Injectable()
export class PromotorService {

  liberaCessao: boolean = false;

  constructor(private _restService: OmniGeralRestService) { }

  getOperador(idPromotor: number): Observable<any> {
    return this._restService.get(`operadores/busca/${idPromotor}`);
  }

  getOperadorLiberaCessao(idPromotor: number) {
    return this.getOperador(idPromotor).pipe(
      take(1))
      .subscribe((res) => (this.promotorLiberaCessao(res.operador.liberaCessao)));
  }

  promotorLiberaCessao(response: boolean) {
    this.liberaCessao = response;
  }

  isOperadorLiberaCessao() {
    return this.liberaCessao;
  }

}
