import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  localStorage = window.localStorage;
  sessionStorage = window.localStorage;
  key = '__storage__';

  patchLocal(data: any) {
    this.localStorage.setItem(this.key, JSON.stringify({
      ...this.getLocal(),
      ...data
    }));
  }

  getLocal(): any {
    const getLocalStorage = this.localStorage.getItem(this.key);
    if (getLocalStorage) {
      return JSON.parse(getLocalStorage) || {};
    }
  }

  removeLocalItem(itemKey: string) {
    const localData = this.getLocal();

    delete localData[itemKey];

    this.patchLocal({ ...localData });
  }

  patchSession(data: any) {
    this.sessionStorage.setItem(this.key, JSON.stringify({
      ...this.getLocal(),
      ...data
    }));
  }

  getSession(): any {
    const getSessionStorage = this.localStorage.getItem(this.key);
    if (getSessionStorage) {
      return JSON.parse(getSessionStorage) || {};
    }
  }

  removeSessionItem(itemKey: string) {
    const localData = this.getSession();

    delete localData[itemKey];

    this.patchSession({ ...localData });
  }
}
