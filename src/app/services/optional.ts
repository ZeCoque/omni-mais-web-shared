export class Optional {

  private value: any;

  public static of(value: any): Optional {
    const instance = new Optional();
    instance.value = value;
    return instance;
  }

  public ifPresentOrElse(consumer: (value: any) => void, emptyAction: () => void): void {
    if (this.value) {
      consumer(this.value);
    } else {
      emptyAction();
    }
  }

  public orElse(other: any) {
    return this.value ? this.value : other;
  }
}
