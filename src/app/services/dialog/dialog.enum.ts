export enum DialogType {
  Success = 1,
  Danger = 2,
  Warning = 3,
  Info = 4,
  Primary = 5,
  Secondary = 6
}
