export interface DialogButton {
  label: string;
  classList?: string[];
  callback?: () => void;
}

export interface Dialog {
  title: string;
  body: string;
  buttons?: DialogButton[];
  classList?: string[];
  textClass?: string;
  iconClass?: string;
  onClose?: () => void;
}
