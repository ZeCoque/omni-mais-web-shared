import { Injectable } from '@angular/core';
import { Dialog, DialogButton } from './dialog.model';
import { dialogTypeCSSClass } from './dialog.map';
import { DialogType } from './dialog.enum';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  nativeElement: HTMLElement;
  title: string;
  body: string;
  textClass: string;
  iconClass: string;
  buttons: DialogButton[];
  classList: string[];
  onClose: () => void;

  setNativeElement(nativeElement: HTMLElement): void {
    this.nativeElement = nativeElement;
  }

  emit({ title, body, buttons, classList, textClass, iconClass, onClose }: Dialog): void {
    this.title = title;
    this.body = body;
    this.buttons = buttons || [];
    this.classList = classList || [];
    this.textClass = textClass || '';
    this.iconClass = iconClass || '';
    if (onClose) {
      this.onClose = onClose;
    }

    document.body.classList.add('modal-open');
    this.nativeElement.classList.add('in');

    this.nativeElement.style.display = 'block';
  }

  close(): void {
    if (!document.querySelectorAll('.modal.in:not(.dialog-modal)').length) {
      document.body.classList.remove('modal-open');
    }

    this.nativeElement.classList.remove('in');

    this.nativeElement.style.display = 'none';
    typeof this.onClose === 'function' && this.onClose.call(this);
  }

  success(body: string, callback?: () => void): void {
    this.emit({
      body,
      title: 'Sucesso',
      buttons: [
        {
          label: 'Ok',
          classList: [`btn-${dialogTypeCSSClass[DialogType.Primary]}`],
          callback: (): void => {
            typeof callback === 'function' && callback.call(this);

            this.close();
          }
        }
      ],
      classList: ['modal-sm']
    });
  }

  error(body: string, callback?: () => void): void {
    this.emit({
      body,
      title: 'Erro',
      buttons: [
        {
          label: 'Ok',
          classList: [`btn-${dialogTypeCSSClass[DialogType.Danger]}`],
          callback: (): void => {
            typeof callback === 'function' && callback.call(this);

            this.close();
          }
        }
      ],
      classList: ['modal-sm']
    });
  }

  warning(body: string, callback?: () => void): void {
    this.emit({
      body,
      title: 'Atenção',
      buttons: [
        {
          label: 'Ok',
          classList: [`btn-${dialogTypeCSSClass[DialogType.Warning]}`],
          callback: (): void => {
            typeof callback === 'function' && callback.call(this);

            this.close();
          }
        }
      ],
      classList: ['modal-sm']
    });
  }

  confirm({ body, title = 'Confirmar', classList = ['modal-sm'], callbackSuccess, callbackError, iconClass, textClass } :
  {
    body: string,
    title?: string,
    classList?: string[],
    iconClass?: string,
    textClass?: string,
    callbackSuccess: () => void,
    callbackError?: () => void
  }): void {
    this.emit({
      body,
      title,
      classList,
      textClass,
      iconClass,
      buttons: [
        {
          label: 'Cancelar',
          classList: ['btn-border-orange'],
          callback: (): void => {
            typeof callbackError === 'function' && callbackError.call(this);

            this.close();
          }
        },
        {
          label: 'Ok',
          classList: ['btn-full-orange'],
          callback: (): void => {
            typeof callbackSuccess === 'function' && callbackSuccess.call(this);

            this.close();
          }
        }
      ]
    });
  }

  info({ body, title = 'Atenção', classList = ['modal-sm'], iconClass, textClass, callbackOK, labelOk = 'Ok' } :
  {
    body: string,
    title?: string,
    classList?: string[],
    iconClass?: string,
    textClass?: string,
    callbackOK?: () => void,
    labelOk?: string
  }): void {
    this.emit({
      body,
      title,
      classList,
      textClass,
      iconClass,
      onClose: callbackOK,
      buttons: [
        {
          label: labelOk,
          classList: ['btn-full-orange'],
          callback: (): void => {
            typeof callbackOK === 'function' && callbackOK.call(this);

            this.close();
          }
        }
      ]
    });
  }
}
