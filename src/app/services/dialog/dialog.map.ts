export const dialogTypeCSSClass = {
  1: 'success',
  2: 'danger',
  3: 'warning',
  4: 'info',
  5: 'primary',
  6: 'secondary'
};
