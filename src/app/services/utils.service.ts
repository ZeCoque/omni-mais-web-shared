import { Injectable } from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class UtilsService {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {}

  get isMobile(): boolean{
    const n = navigator.userAgent;
    return /Android|webOS|iPhone|iPad|iPod|Windows Phone|IEMobile|Opera Mini|Mobile|mobile|Tablet|CriOS/i.test(
      n
    );
  }

  registerIcons(icons: string[], iconFolder: string): void {
    icons.forEach((icone) => {
      this.matIconRegistry.addSvgIcon(
        icone,
        this.domSanitizer.bypassSecurityTrustResourceUrl(
          iconFolder + `/${icone}.svg`
        )
      );
    });
  }

  timeoutPromise(timeout: number) {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        resolve(); }, timeout);
    });
  }

  applyCPFMask(cpf: string): string {
    if (!cpf) {
      return '';
    }
    return `${cpf.substring(0, 3)}.${cpf.substring(3, 6)}.${cpf.substring(
      6,
      9
    )}-${cpf.substring(9, 11)}`;
  }

}
