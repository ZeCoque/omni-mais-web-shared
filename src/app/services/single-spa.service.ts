
import { Injectable } from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';

@Injectable({
  providedIn: 'any',
})
export class SingleSpaService {

  public assetUrl(url: string): string {
    return assetUrl(url);
  }
}
