import { Injectable } from '@angular/core';
import {
  MatBottomSheet,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { last } from 'lodash';
import { ModalQuestionComponent } from '../components/modal-question/modal-question.component';
import { BottomSheetComponent, DialogComponent } from '../components/modal/modal.component';
import {
  ModalOptions,
  ModalQuestionOptions,
} from '../models/modal-options.model';
@Injectable({
  providedIn: 'root',
})
export class ModalService {
  innerWidth = 600;
  refs = [];

  constructor(private sheet: MatBottomSheet, private dialog: MatDialog) {}

  public showModal(
    options: ModalOptions,
    params: any = {}
  ): MatDialogRef<DialogComponent> {
    return this.getModal(options, params);
  }

  public showSheet(
    options: ModalOptions,
    params: any = {}
  ): MatBottomSheetRef<BottomSheetComponent> {
    return this.getSheet(options, params);
  }

  public show(
    options: ModalOptions,
    params: any = {}
  ): MatDialogRef<DialogComponent> | MatBottomSheetRef<BottomSheetComponent> | any {
    if (window.innerWidth > this.innerWidth) {
      return this.getModal(options, params);
    }
    return this.getSheet(options, params);
  }

  public close(value?: any) {
    const ref = last(this.refs);
    if (!ref) {
      return;
    }
    ref.modal[ref.close](value);
  }

  public getData() {
    const ref = last(this.refs);
    if (!ref) {
      return;
    }
    return ref.data;
  }

  public create(
    component: any,
    options: any = {},
    params: any = {}
  ): MatDialogRef<any> | MatBottomSheetRef<any> | any {
    if (window.innerWidth > this.innerWidth) {
      return this.componentModal(component, options, {
        width: '480px',
        maxHeight: `${window.innerHeight - 50}px`,
        data: options,
        autoFocus: false,
        disableClose: true,
        ...params,
      });
    }

    return this.componentSheet(component, options, {
      data: options,
      autoFocus: false,
      disableClose: true,
      ...params,
    });
  }

  public createModal(
    component: any,
    options: any = {},
    params: any = {}
  ): MatDialogRef<any> | any {
    return this.componentModal(component, options, {
      width: '480px',
      maxHeight: `${window.innerHeight - 50}px`,
      data: options,
      autoFocus: false,
      disableClose: true,
      ...params,
    });
  }

  public createSheet(
    component: any,
    options: any = {},
    params: any = {}
  ): MatBottomSheetRef<any> | any {
    return this.componentSheet(component, options, {
      data: options,
      autoFocus: false,
      disableClose: true,
      ...params,
    });
  }

  private getModal(
    options: ModalOptions,
    params: any = {}
  ): MatDialogRef<DialogComponent> {
    return this.componentModal(DialogComponent, options, {
      width: '480px',
      maxHeight: `${window.innerHeight - 50}px`,
      data: options,
      autoFocus: false,
      disableClose: true,
      ...params,
    });
  }

  private getSheet(
    options: ModalOptions,
    params: any = {}
  ): MatBottomSheetRef<BottomSheetComponent> {
    return this.componentSheet(BottomSheetComponent, options, {
      data: options,
      autoFocus: false,
      disableClose: true,
      ...params,
    });
  }

  private componentSheet(
    component: any,
    options: any,
    params: any = {}
  ): MatBottomSheetRef<any> | any {
    const data = { ...options };
    const ref: any = { data };
    this.refs.push(ref);
    const item = last(this.refs);
    item.modal = this.sheet.open(component, { ...params, data });
    item.close = 'dismiss';
    item.modal.afterDismissed().subscribe((result) => {
      if (options.callback) {
        options.callback(result);
      }
      const indexRef = this.refs.findIndex((f) => f.close === 'dismiss');
      if (indexRef >= 0) {
        this.refs.splice(indexRef, 1);
      }
    });
    return item.modal;
  }

  private componentModal(
    component: any,
    options: any,
    params: any = {}
  ): MatDialogRef<any> | any {
    const data = { ...options };
    const ref: any = { data };
    this.refs.push(ref);
    const item = last(this.refs);
    item.modal = this.dialog.open(component, { ...params, data });
    item.close = 'close';
    item.modal.afterClosed().subscribe((result) => {
      if (options.callback) {
        options.callback(result);
      }
      if (this.refs) {
        this.refs.pop();
      }
    });
    return item.modal;
  }

  public showQuestion(
    options: ModalQuestionOptions,
    params: any = {}
  ): MatDialogRef<DialogComponent> | MatBottomSheetRef<BottomSheetComponent> | any {
    this.getModalQuestion(options, params);
  }

  private getModalQuestion(
    options: ModalQuestionOptions,
    params: any = {}
  ): MatDialogRef<ModalQuestionComponent> {
    return this.componentModal(ModalQuestionComponent, options, {
      width: '280px',
      maxHeight: `${window.innerHeight - 50}px`,
      data: options,
      autoFocus: false,
      disableClose: true,
      ...params,
    });
  }
}
