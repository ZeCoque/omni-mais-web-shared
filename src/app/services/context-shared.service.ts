import { Injectable } from '@angular/core';
import { Optional } from './optional';

@Injectable()
export class ContextSharedService {
  private context: any;

  constructor() {}

  public set(context: any) {
    this.context = context;
  }

  public get(): Optional {
    const value = this.context;
    this.context = undefined;
    return Optional.of(value);
  }
}
