export class TelefonePessoa {

  private idCategoria: number | undefined;
  private idTipoTelefone: number | undefined;
  private ddd: string | undefined;
  private numero: string | undefined;

  constructor(idCategoria?: number, idTipoTelefone?: number, ddd?: string, numero?: string) {
    this.idCategoria = idCategoria;
    this.idTipoTelefone = idTipoTelefone;
    this.ddd = ddd;
    this.numero = numero;
  }

  setIdCategoria(idCategoria: number) {
    this.idCategoria = idCategoria;
  }

  setIdTipoTelefone(idTipoTelefone: number) {
    this.idTipoTelefone = idTipoTelefone;
  }

  setDdd(ddd: string) {
    this.ddd = ddd;
  }

  setNumero(numero: string) {
    this.numero = numero;
  }

  getDdd(): string | undefined {
    return this.ddd;
  }

  getNumero(): string | undefined {
    return this.numero;
  }

}
