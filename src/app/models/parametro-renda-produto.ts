export class ParametroRendaProduto {
  valorRendaMinima: number;
  valorSolicitadoMaximo: number;
  valorSolicitadoMinimo: number;
}
