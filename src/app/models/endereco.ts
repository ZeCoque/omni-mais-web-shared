export class Endereco {

  private idTipoEndereco: number;
  private uf: string;
  private cidade: string;
  private endereco: string;
  private bairro: string;
  private numero: number;
  private complemento: string;
  private cep: string;

  setIdTipoEndereco(idTipoEndereco: number) {
    this.idTipoEndereco = idTipoEndereco;
  }

  setUf(uf: string) {
    this.uf = uf;
  }

  setCidade(cidade: string) {
    this.cidade = cidade;
  }

  setEndereco(endereco: string) {
    this.endereco = endereco;
  }

  setBairro(bairro: string) {
    this.bairro = bairro;
  }

  setNumero(numero: number) {
    this.numero = numero;
  }

  setComplemento(complemento: string) {
    this.complemento = complemento;
  }

  setCep(cep: string) {
    this.cep = cep;
  }
}
