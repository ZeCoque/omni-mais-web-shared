import { Cliente } from './cliente';
import { IdDescricao } from './id-descricao';
import { ParametroRendaProduto } from './parametro-renda-produto';

export interface Proposta {
  contratoId?: string;
  dataHoraEnvio?: number;
  assistencia?: number;
  assistencias?: any[];
  carencia: number;
  cliente?: Cliente;
  conjuge?: Cliente;
  documentos?: [];
  experienciasCredito?: IdDescricao[];
  id?: number;
  indexador?: string;
  lojista?: any;
  mercadorias?: PropostaMercadoria[];
  modalidadeFormalizacao?: number;
  mostra?: boolean;
  observacoes?: string;
  operacao?: any;
  plataformaInclusao: 'WEB' | 'IOS' | 'ANDROID';
  ppe?: boolean;
  qtdeParcelas?: number;
  seguro?: number;
  seguros?: any[];
  simulacao?: { id: number };
  statusAnaliseCpf?: string;
  tabela?: IdDescricao;
  valorEntrada?: number;
  valorLiquido?: number;
  valorParcela?: number;
  valorProposta?: number;
  comoAjuda?: string;
  favorecido?: any;
  codigoMotivoDesistencia?: number;
  transients?: any;
  possuiSosInversoEmAberto?: boolean;
  produto?: string;
  vendedor?: { id: number, nome?: string, nmLoja?: string };
  promotor?: { id: number, nome?: string };
  rastreioStatus?: { codigo: number, descricao: string};
  dataEmissaoContrato?: number;
  avalistas?: { [key: number]: Cliente };
  motivoRecusaAssinaturaDigital?: any;
  grupo2?: string;
  etapaAtual?: number;
  status?: number;
  parametroRendaProduto?: ParametroRendaProduto;
}

export interface PropostaMercadoria {
  mercadoria?: string;
  tipoMercadoria?: number;
  valor?: number;
}
