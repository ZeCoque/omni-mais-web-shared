import { Telefone } from './telefone';
import { Endereco } from './endereco';

export class DadoProfissional {

  private idClasseProfissional: number;
  private idProfissao: number;
  private nomeEmpresa: string;
  private endereco: Endereco;
  private telefone: Telefone;
  private valorPatrimonio: number;

  setIdClasseProfissional(idClasseProfissional: number) {
    this.idClasseProfissional = idClasseProfissional;
  }

  setIdProfissao(idProfissao: number) {
    this.idProfissao = idProfissao;
  }

  setNomeEmpresa(nomeEmpresa: string) {
    this.nomeEmpresa = nomeEmpresa;
  }

  setEndereco(endereco: Endereco) {
    this.endereco = endereco;
  }

  setTelefone(telefone: Telefone) {
    this.telefone = telefone;
  }

  setValorPatrimonio(valorPatrimonio: number) {
    this.valorPatrimonio = valorPatrimonio;
  }
}
