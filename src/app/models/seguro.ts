export class Seguro {
  private id: number | undefined;
  private valor: number | undefined;

  constructor(id?: number, valor?: number) {
    this.id = id;
    this.valor = valor;
  }
}
