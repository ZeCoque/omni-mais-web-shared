export class Telefone {
  private ddd: string | undefined;
  private numero: string | undefined;

  constructor(ddd?: string, numero?: string) {
    this.ddd = ddd;
    this.numero = numero;
  }
}
