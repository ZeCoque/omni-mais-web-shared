import { IdDescricao } from './id-descricao';

export interface Cliente {
  cpf?: string;
  dadosProfissionais?: ClienteDadosProfissional;
  dtNascimento?: number;
  email?:	string;
  enderecos?: ClienteEndereco[];
  estadoCivil?: IdDescricao;
  id?: number;
  nacionalidade?: IdDescricao;
  naturalDe?: string;
  naturalDeUf?: string;
  nomeCliente?: string;
  nomeMae?: string;
  nomePai?: string;
  referencias?:	ClienteReferencia[];
  rendas?: ClienteRenda[];
  score?: { valor: number };
  sexo?: 'M' | 'F';
  telefones?: ClienteTelefone[];
  tipoDocumento?:	'RG' | 'CARTEIRA_DE_CLASSE' | 'CTPS' | 'CNH';
  tipoDocumentoDataEmissao?: any;
  tipoDocumentoEmissor?:	string;
  tipoDocumentoNumero?: string;
  valorTotalPatrimonio?: number;
  ultimaFichaAberta?: number;
  conjuge?: Cliente;
  banco?: String;
  dependentes?: number;
  grauInstrucao?: number;
  tipoMoradia?: number;
  tempoResidencia?: number;
  ppe?: boolean;
  transients?: { [key:string]: any };
  grauParentesco?: number;
}

export interface ClienteRenda {
  empresa?: string;
  empresaDdd?: string;
  empresaFone?: string;
  empresaRamal?: string;
  rendaAtribuida?:	boolean;
  tipoRenda: 'SALARIO' | 'ALUGUEL' | 'ACOES_DIVIDENDOS' | 'PENSAO' | 'APOSENTADORIA' | 'JUROS' | 'PRO_LABORE' | 'OUTRA';
  valor:	number;
}

export interface ClienteEndereco {
  bairro: string;
  cep:	string;
  cidade:	string;
  complemento?:	string;
  correspondencia:	boolean;
  logradouro:	string;
  numero?:	number;
  tipoEndereco: 'RESIDENCIAL' | 'COMERCIAL' | 'FAMILIAR' | 'OUTROS';
  uf: string;
}

export interface ClienteTelefone {
  categoriaTelefone?: 'RESIDENCIAL' | 'COMERCIAL' | 'CELULAR' | 'RECADO' | 'CELULAR_ADICIONAL';
  ddd?: string;
  nrTelefone?:	string;
  tipoTelefone?:	'PROPRIO' | 'RECADOS' | 'ALUGADO' | 'CELULAR';
}

export interface ClienteReferencia {
  ddd: string;
  nrTelefone:	string;
  nome: string;
  tipoReferencia: IdDescricao;
}

export interface ClienteDadosProfissional {
  classeProfissional?: {
    descricao?: string
    grupoProfissao?:	number;
    id?:	number;
  };
  nomeEmpresa?:	string;
  profissao?: {
    id?: number;
    idGrupoProfissao?:	number;
  };
  tempoServico?: number;
  salario?: number;
}
