import { NgModule } from '@angular/core';
import { BtnGroupComponent } from './btn-group.component';

@NgModule({
  declarations: [
    BtnGroupComponent
  ],
  exports: [
    BtnGroupComponent
  ]
})
export class BtnGroupModule {}
