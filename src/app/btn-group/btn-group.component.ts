import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-btn-group',
  templateUrl: './btn-group.component.html',
  styleUrls: ['./btn-group.component.css']
})
export class BtnGroupComponent implements OnInit {

  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
