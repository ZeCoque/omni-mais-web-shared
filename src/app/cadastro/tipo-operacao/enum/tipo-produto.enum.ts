export class TipoProdutoEnum {
  public static FINANCIMENTO = 'CDC';
  public static REFINANCIAMENTO = 'REFINANCIAMENTO';
  public static CDC_LOJA = 'CDC LOJA';
  public static CDC_PREMIUM = 'CDC PREMIUM';
  public static CREDITO_PESSOAL = 'CREDITO_PESSOAL';
  public static MICROCREDITO = 'MICROCREDITO';
}
