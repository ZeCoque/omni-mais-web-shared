export class TipoNegociacaoEnum {
  public static CARNE = 'CRÉDITO PESSOAL-CN';
  public static DEBITO_AUTOMATICO = 'CRÉDITO PESSOAL-DA';
}
