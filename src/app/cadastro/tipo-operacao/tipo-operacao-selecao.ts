export class TipoOperacaoSelecao {
  tipoProduto: string;
  operacao: string;
  produto: string;
  banco: string;
  parametroRendaProduto?: ParametroRendaProduto;
}

export class ParametroRendaProduto {
  valorRendaMinima: number;
  valorSolicitadoMaximo: number;
  valorSolicitadoMinimo: number;
}
