import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from '../breadcrumb/breadcrumb.module';
import { ModalsModule } from '../modals/modals.module';
import { ContextSharedService } from '../services/context-shared.service';
import { PropostaStateService } from '../services/proposta-state.service';
import { StorageService } from '../services/storage.service';
import { ButtonSelectorComponent } from './button-selector/button-selector.component';
import { CadastroComponent } from './cadastro.component';
import { QuemSolicitouComponent } from './quem-solicitou/quem-solicitou.component';
import { TipoOperacaoComponent } from './tipo-operacao/tipo-operacao.component';

@NgModule({
  imports: [
    CommonModule,
    BreadcrumbModule,
    FormsModule,
    ReactiveFormsModule,
    ModalsModule,
  ],
  declarations: [
    CadastroComponent,
    QuemSolicitouComponent,
    TipoOperacaoComponent,
    ButtonSelectorComponent,
  ],
  exports: [
    CadastroComponent,
    QuemSolicitouComponent,
    TipoOperacaoComponent,
    ButtonSelectorComponent,
  ],
  providers: [
    ContextSharedService,
    PropostaStateService,
    StorageService
  ]
})
export class CadastroModule { }
