import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonSelectorData } from './button-selector-data';

@Component({
  selector: 'app-button-selector',
  templateUrl: './button-selector.component.html',
  styleUrls: ['./button-selector.component.css']
})
export class ButtonSelectorComponent implements OnInit {

  @Input() data: ButtonSelectorData;
  @Input() selected: ButtonSelectorData;
  @Output() selectedChange = new EventEmitter<ButtonSelectorData>();

  constructor() { }

  ngOnInit() {
  }

  onClickSelect() {
    this.selected = !this.selected ? this.data : this.data.id === this.selected.id ? new ButtonSelectorData() : this.data;
    this.selectedChange.emit(this.selected);
  }

}
