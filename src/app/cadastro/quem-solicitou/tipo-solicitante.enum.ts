export class TipoSolicitanteEnum {
  public static VENDEDOR = 'VENDEDOR';
  public static CLIENTE = 'CLIENTE';
  public static OUTRA = 'OUTRA';
  public static LOJA = 'LOJA';
}
