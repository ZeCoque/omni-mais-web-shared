import { ParametroRendaProduto } from './tipo-operacao/tipo-operacao-selecao';

export class NovaProposta {
  tipoProduto: string;
  produto: string;
  banco: string;
  operacao: string;
  solicitante: string;
  parametroRendaProduto?: ParametroRendaProduto;
}
