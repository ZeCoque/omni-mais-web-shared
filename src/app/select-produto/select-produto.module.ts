import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BtnGroupModule } from '../btn-group/btn-group.module';
import { PanelModule } from '../dashboard/panel/panel.module';
import { OmniRestModule } from '../omni-rest/omni-rest.module';
import { ProdutoStorage } from './produto.storage';
import { SelectProdutoComponent } from './select-produto.component';
import { SelectProdutoService } from './select-produto.service';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    FormsModule,
    BtnGroupModule,
    OmniRestModule,
  ],
  declarations: [SelectProdutoComponent],
  exports: [SelectProdutoComponent],
  providers: [
    SelectProdutoService,
    ProdutoStorage]
})
export class SelectProdutoModule { }
