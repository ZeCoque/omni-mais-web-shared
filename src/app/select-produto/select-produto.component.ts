
import { map } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProdutoStorage } from './produto.storage';
import { SelectProdutoService } from './select-produto.service';
import { environment } from 'src/environments/environment';
import { Produto } from './model/produto';
import { isEmptyObj } from '../utils/utils';

@Component({
  selector: 'app-select-produto',
  templateUrl: './select-produto.component.html',
  styleUrls: ['./select-produto.component.css']
})
export class SelectProdutoComponent implements OnInit {
  produtos: any;
  selecionados: any;
  isAllSelected: boolean = false;

  @Input() disabled = false;
  @Input() isPanel: false;
  @Output() filtrarPorProduto: EventEmitter<any> = new EventEmitter();

  constructor(private service: SelectProdutoService, private produtoStorage: ProdutoStorage) {
    this.selecionados = [];
  }

  ngOnInit() {
    this.service.getProdutos()
      .pipe(map(this.checkProduto))
      .subscribe((produtos) => {
        this.produtos = produtos.filter(item => environment.SHOW_CP || item.descricao !== 'CRÉDITO PESSOAL');
        if (!isEmptyObj(this.produtoStorage.tipoOperacao)) {
          this.produtos = this.produtos.map((produto: { checked: Produto | undefined; descricao: string; }) => {
            produto.checked = this.produtoStorage.tipoOperacao.find(value => value.descricao === produto.descricao);
            return produto;
          });
        }
        this.selectAll();
      });
  }

  checkProduto(res: { descricao: any; }[]) {
    return res.map((value: { descricao: any; }) => {
      return {
        descricao: value.descricao,
        checked: true
      };
    });
  }

  saveProdutosStorage() {
    if (isEmptyObj(this.produtoStorage.tipoOperacao)) {
      this.produtoStorage.save(this.produtos);
    }
    this.selecionados = this.produtoStorage.tipoOperacao;
    this.emitProduto(this.selecionados);
  }

  selectAll() {
    if (!this.isAllSelected) {
      this.produtos = this.produtos.map((item: any) => {
        return { ...item, checked: false };
      });

      this.selecionados = [];
      this.produtoStorage.save(this.selecionados);
      this.emitProduto(this.selecionados);
    }
    this.isAllSelected = true;
  }

  select(produtoDOM: MouseEvent, produtoObj: { checked: any; descricao: string}) {
    this.isAllSelected = false;

    const prod = {
      ...produtoObj,
      checked: !produtoObj.checked
    };

    if (!produtoDOM.target) return;

    const target: any = produtoDOM.target;

    target.checked
      ? this.selecionados.push(prod)
      : this.selecionados = this.selecionados
          .filter((ele: { descricao: any; }) => ele.descricao !== prod.descricao);

    this.produtoStorage.save(this.selecionados);
    this.emitProduto(this.selecionados);
  }

  private emitProduto(produtosSelecionados: any[]): void {
    const produtos = produtosSelecionados.map((produto: { descricao: string; }) => {
      return {
        ...produto,
        descricaoNormalized: produto.descricao.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
      };
    });

    this.filtrarPorProduto.emit(produtos);
  }

}
