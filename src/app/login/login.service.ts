
import { throwError as observableThrowError,  Observable } from 'rxjs';

import { tap, map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { OmniMaisRestService } from '../omni-rest/omni-mais/omni-mais-rest.service';
import { OmniWsRestService } from '../omni-rest/omni-ws/omni-ws-rest.service';
import { LoginCredencial } from './model/login-credencial';
import { LoginResult } from './model/login-result';

@Injectable()
export class LoginService {

  constructor(
    private omniWsRestService: OmniWsRestService,
    private omniMaisRestService: OmniMaisRestService
  ) { }

  login(credencial: LoginCredencial): Observable<LoginResult> {
    if (credencial.login && credencial.senha) {
      return this.omniWsRestService.post('auth/user', { usuarioTO: credencial }).pipe(
        map(res => res as LoginResult),
        tap(res => {
          if (!res.status || !res.status.codigo || res.status.codigo !== 1) {
            throw new Error(res.status.descricao || 'Usuário ou Senha inválida');
          }
        }),
        catchError(error => observableThrowError('Usuário ou Senha inválida')));
    }
    return observableThrowError('Informe Usuário e Senha para realizar o Login');
  }

  recoveryPassword(credencial: LoginCredencial): Observable<any> {
    if (credencial.login) {
      return this.omniWsRestService.post('auth/user/esqueci', { usuarioTO: credencial });
    }
    return observableThrowError('Informe o Usuário para realizar a recuperação de sua senha');
  }

  getAndAuthenticateUser(usuarioTO: any) {
    return this.omniWsRestService
      .post('auth/user/token', { usuarioTO }).pipe(
      map(res => res as LoginResult),
      tap(res => {
        if (!res.status || !res.status.codigo || res.status.codigo !== 1) {
          throw new Error(res.status.descricao);
        }
      }),
      catchError(error => observableThrowError('Login inválido ou sessão expirada')));
  }

  selecionarAgente(idAgente: string) {
    return this.omniMaisRestService.post('usuario/selecionarAgentePorUsuario', { idAgente });
  }
}
