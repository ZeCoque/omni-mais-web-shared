import { Injectable } from '@angular/core';
import { LoginAgente } from './model/login-agente';
import { LoginUsuarioTO } from './model/login-usuario-to';

const USER = 'id_session';
const AGENTE = 'id_session_ag';

@Injectable()
export class LoginStorage {

  constructor() { }

  register(usuario: LoginUsuarioTO) {
    this.setStorage(USER, usuario);
  }

  registerAgente(agente: LoginAgente) {
    this.setStorage(AGENTE, agente);
  }

  clear(): void {
    sessionStorage.removeItem(USER);
    sessionStorage.removeItem(AGENTE);
  }

  isContainsToken(): boolean {
    return !!this.usuario.token && this.usuario.token !== 'undefined' && this.usuario.token.trim().length > 0;
  }

  get token(): string {
    return this.usuario.token;
  }

  get login(): string {
    return this.usuario.login;
  }

  get loginAndTokenAsJson(): string {
    const usuario: LoginUsuarioTO = this.usuario;
    return JSON.stringify({ login: usuario.login, token: usuario.token });
  }

  get usuario(): LoginUsuarioTO {
    return new LoginUsuarioTO(this.getStorage(USER));
  }

  get agente(): LoginAgente {
    return this.getStorage(AGENTE);
  }

  private setStorage(key: string, value: any) {
    sessionStorage.setItem(key, btoa(JSON.stringify(value)));
  }

  private getStorage(key: string): any {
    const value = sessionStorage.getItem(key);
    if (value) {
      return JSON.parse(atob(value));
    }
    return {};
  }
}
