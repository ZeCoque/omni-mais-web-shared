import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoaderModule } from '../loaders/loaders.module';
import { ModalsModule } from '../modals/modals.module';
import { PromotorService } from '../services/promotor.service';
import { AuthenticationComponent } from './authentication/authentication.component';
import { LoginComponent } from './login.component';
import { LoginGuard } from './login.guard';
import { LoginService } from './login.service';
import { LoginStorage } from './login.storage';
import { SelectAgenteComponent } from './select-agente/select-agente.component';

@NgModule({
  imports: [
    CommonModule,
    ModalsModule,
    FormsModule,
    LoaderModule,
  ],
  declarations: [
    LoginComponent,
    AuthenticationComponent,
    SelectAgenteComponent
  ],
  exports: [
    LoginComponent,
    AuthenticationComponent,
    SelectAgenteComponent
  ],
  providers: [
    LoginService,
    LoginGuard,
    LoginStorage,
    PromotorService
  ],
})
export class LoginModule { }
