export class LoginAgente {
  id: number;
  nome: string;
  lojaBalcaoId: number;
  cidade: string;
  uf: string;
}
