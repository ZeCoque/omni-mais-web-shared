import { LoginUsuarioTO } from './login-usuario-to';
import { LoginStatus } from './login-status';

export class LoginResult {
  usuarioTO: LoginUsuarioTO;
  status: LoginStatus;
}
