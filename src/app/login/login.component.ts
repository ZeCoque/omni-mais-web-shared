import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from './../../environments/environment.prod';
import { LoginAction } from './login-action.enum';
import { LoginService } from './login.service';
import { LoginStorage } from './login.storage';
import { LoginCredencial } from './model/login-credencial';
import { SelectAgenteResult } from './select-agente/select-agente-result';
import { SelectAgenteResultStatus } from './select-agente/select-agente-result-status';
import { SelectAgenteComponent } from './select-agente/select-agente.component';
import { ToastrService } from 'ngx-toastr';
import { ProdutoStorage } from '../select-produto/produto.storage';
import { LoginResult } from './model/login-result';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @ViewChild('selecionarAgente', { static: true }) selecionarAgente: SelectAgenteComponent;

  credencial: LoginCredencial = new LoginCredencial();
  message: string;
  public version = environment.VERSION_APP;
  private action: LoginAction = LoginAction.LOGIN;

  isLoggingIn = false;

  constructor(
    private loginService: LoginService,
    private loginStorage: LoginStorage,
    private router: Router,
    private actRoute: ActivatedRoute,
    private produtoStorage: ProdutoStorage,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.loginStorage.clear();
    this.produtoStorage.clear();
    this.initSelectAgenteSubscribe();
  }

  initSelectAgenteSubscribe() {
    this.selecionarAgente.onSelectAgente.subscribe(result =>
      this.onSelectAgente(result),
    );
  }

  isLogin(): boolean {
    return this.action === LoginAction.LOGIN;
  }

  isRecoveryPassword(): boolean {
    return this.action === LoginAction.RECOVERY_PASSWORD;
  }

  isMessage(): boolean {
    return this.action === LoginAction.MESSAGE;
  }

  back(): void {
    this.action = LoginAction.LOGIN;
  }

  recoveryPassword(): void {
    this.credencial.senha = undefined;
    this.action = LoginAction.RECOVERY_PASSWORD;
  }

  private showMessage(message: string | undefined): void {
    this.toastr.error(message, 'Falha ao efetuar o login');
    this.isLoggingIn = false;
  }

  login(): void {
    if (!this.credencial.login) return;
    this.credencial.login = this.credencial.login.trim();
    this.isLoggingIn = true;
    this.credencial.versaoAplicacao = environment.VERSION_APP;
    this.loginService.login(this.credencial).subscribe(
      (result) => {
        this.handleUser(result);
      },
      error => this.showMessage(error),
    );
  }

  handleUser(userData: LoginResult) {
    if ('promotor' in userData.usuarioTO || userData.usuarioTO.tipoUsuarioTO.descricao === 'LOJISTA') {
      this.loginStorage.register(userData.usuarioTO);
      this.selecionarAgente.selecionar();
    } else {
      this.toastr.show('Desculpe, você não é um operador :(');
      this.isLoggingIn = false;
    }
  }

  sendRecoveryPassword(): void {
    this.loginService
      .recoveryPassword(this.credencial)
      .subscribe(
        result =>
          this.showMessage(
            'Uma senha provisória foi enviada para seu e-mail '
          ),
        error =>
          this.showMessage(
            'Não foi possível atribuir uma nova senha para o usuário informado. ' +
              'Favor informar novamente ou contatar o Suporte Técnico.'
          ),
      );
  }

  onKeyup(event: { keyCode: number; }) {
    if (event.keyCode !== 13) {
      return;
    }
    if (this.action === LoginAction.LOGIN) {
      this.login();
    }
    if (this.action === LoginAction.RECOVERY_PASSWORD) {
      this.sendRecoveryPassword();
    }
  }

  selectRouter() {
    this.actRoute.queryParams.subscribe((params) => {
      if ('route' in params) {
        this.router.navigate([`/${params.route}`]);
      } else {
        this.router.navigate(['/ficha']);
      }
    });
  }

  private onSelectAgente(result: SelectAgenteResult) {
    switch (result.status) {
      case SelectAgenteResultStatus.SUCCESS:
        this.selectRouter();
        break;
      case SelectAgenteResultStatus.ERROR:
        this.showMessage(
          'Falha na Comunicação, verifique sua conexão.'
        );
        break;
      default:
        this.isLoggingIn = false;
    }
  }
}
