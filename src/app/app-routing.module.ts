import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FichaListaComponent } from './fichas/ficha-lista/ficha-lista.component';
import { AuthenticationComponent } from './login/authentication/authentication.component';
import { LoginComponent } from './login/login.component';
import { LoginGuard } from './login/login.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'authentication', component: AuthenticationComponent },
  {
    path: 'ficha',
    component: FichaListaComponent,
    canActivate: [LoginGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
