import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AngularMaterialModule } from './angular-material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DialogComponent } from './dialog/dialog.component';
import { FichasModule } from './fichas/fichas.module';
import { LoaderModule } from './loaders/loaders.module';
import { LoginModule } from './login/login.module';
import { MenuSharedService } from './menu/menu-shared.service';
import { MenuComponent } from './menu/menu.component';
import { ModalsModule } from './modals/modals.module';
import { OmniRestModule } from './omni-rest/omni-rest.module';
import { ProdutoStorage } from './select-produto/produto.storage';
import { SelectProdutoModule } from './select-produto/select-produto.module';
import { DialogService } from './services/dialog/dialog.service';

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
    MenuComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    AngularMaterialModule,
    LoginModule,
    ModalsModule,
    LoaderModule,
    SelectProdutoModule,
    ToastrModule.forRoot(),
    FichasModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    DialogService,
    ProdutoStorage,
    MenuSharedService,
    OmniRestModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
