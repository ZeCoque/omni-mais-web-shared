export const normalizeList = (list: any): any[] => Array.prototype.slice.call(list);

export const compose = (...functions: any[]) => (args: any) => functions.reduce((arg, fn) => fn(arg), args);

export const clearJSONReferences = (object: any) => JSON.parse(JSON.stringify(object));
