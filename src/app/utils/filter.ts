import { FichaListaFiltro } from "../fichas/model/ficha-lista-filtro";

export function fichasFilter(ngFilter: FichaListaFiltro, data: any[]) {
  let status: string | undefined = ngFilter.status;

  if (status === '-1') {
    status = undefined;
  }

  if (data && data.length) {
    return data.filter((item: { vendedor: { nmLoja: string; }; cliente: { nome: string; cpf: string; }; id: { toString: () => string | any[]; }; statusTimeline: { idStatus: number; }; produto: string; dataHoraEnvio: string | number; }) => {
      if (ngFilter.nomeLoja && (!item.vendedor || !item.vendedor.nmLoja.toLowerCase().includes(ngFilter.nomeLoja.toLowerCase()))) {
        return false;
      }

      if (ngFilter.nomeCpf && !item.cliente.nome.toLowerCase().includes(ngFilter.nomeCpf.toLowerCase()) &&
        !item.cliente.cpf.toLowerCase().includes(ngFilter.nomeCpf.toLowerCase())) {
        return false;
      }

      if (ngFilter.numero && item.id.toString().indexOf(ngFilter.numero.toString()) === -1) {
        return false;
      }

      if (status && item.statusTimeline.idStatus !== parseInt(status)) {
        return false;
      }

      if (ngFilter.sos && !JSON.stringify(item.statusTimeline).includes('SOS')) {
        return false;
      }

      if (item.produto === 'VEÍCULOS') {
        if (ngFilter.data) {
          const days = (24 * 60 * 60 * 1000) * +ngFilter.data;
          if (+item.dataHoraEnvio < +(Date.now() - days)) {
            return false;
          }
        }
      } else {
        return item.statusTimeline.idStatus !== 16;
      }

      return true;
    });
  }
  return data;
}

export function calcularQuantidadeSOS(data: any[]) {
  const quantidadeSOS = data.filter((item: { totalSosPendente: number; }) => {
    return item.totalSosPendente !== 0;
  });
  return quantidadeSOS.length;
}
