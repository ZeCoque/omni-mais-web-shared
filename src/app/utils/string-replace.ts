export class StringReplace {
  public static removeSpaces(value: string) {
    return (value || '').replace(/\s/g, '');
  }

  public static removeAccentuation(value: string) {
    return (value || '').normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }
}
