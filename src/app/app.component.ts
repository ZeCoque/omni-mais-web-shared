
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { LoginStorage } from './login/login.storage';
import { ProdutoStorage } from './select-produto/produto.storage';

declare var gtag:Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  private currentRoute: string;
  routerSubscription: any;

  constructor(
      private router: Router,
      private loginStorage: LoginStorage,
      private produtoStorage: ProdutoStorage) {

    router.events.pipe(distinctUntilChanged((previous: any, current: any) => {
      if (current instanceof NavigationEnd) {
        return previous.url === current.url;
      }
      return true;
    })).subscribe((x: any) => {
      gtag('config', 'UA-144762582-3', { page_path: x.url });
    });
  }

  ngOnInit() {
    this.routerSubscription = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd))
      .subscribe((event: any): void => {
        this.currentRoute = event.urlAfterRedirects;
      });
  }

  ngOnDestroy() {
    this.loginStorage.clear();
    this.produtoStorage.clear();
    this.routerSubscription.unsubscribe();
  }

  isShowMenu() {
    return this.currentRoute &&
      !this.currentRoute.includes('/login') &&
      !this.currentRoute.includes('/authentication') &&
      !this.currentRoute.includes('/verificacao-seguranca');
  }
}
