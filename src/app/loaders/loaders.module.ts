import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CentralLoaderComponent } from './central-loader/central-loader.component';
import { LoaderCirclesComponent } from './loader-circles/loader-circles.component';
import { LoaderComponent } from './loader/loader.component';
import { LoaderService } from './loader/loader.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CentralLoaderComponent,
    LoaderComponent,
    LoaderCirclesComponent,
  ],
  providers: [
    LoaderService
  ],
  exports: [
    CentralLoaderComponent,
    LoaderComponent,
    LoaderCirclesComponent
  ]
})
export class LoaderModule { }
