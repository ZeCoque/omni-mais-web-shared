import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-central-loader',
  templateUrl: './central-loader.component.html',
  styleUrls: ['./central-loader.component.css']
})
export class CentralLoaderComponent implements OnInit {

  @Input() text: string;
  @Input() show = true;

  constructor() { }

  ngOnInit() {
  }

}
