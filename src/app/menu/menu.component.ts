import { take } from 'rxjs/operators';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectAgenteResult } from '../login/select-agente/select-agente-result';
import { environment } from './../../environments/environment';
import { LoginStorage } from './../login/login.storage';
import { SelectAgenteResultStatus } from '../login/select-agente/select-agente-result-status';
import { SelectAgenteComponent } from '../login/select-agente/select-agente.component';
import { MenuSharedService } from './menu-shared.service';
import { SelectProdutoService } from '../select-produto/select-produto.service';
import { ModalMessageComponent } from '../modals/modal-message/modal-message.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  public version = environment.VERSION_APP;
  @ViewChild('selecionarAgente', { static: true }) selecionarAgente: SelectAgenteComponent;
  @ViewChild('modalMessage', { static: true }) modalMessage: ModalMessageComponent;

  loginStorage: LoginStorage;
  produtos: any[];

  constructor(
    loginStorage: LoginStorage,
    private sharedService: MenuSharedService,
    private produtoService: SelectProdutoService
  ) {
    this.loginStorage = loginStorage;
  }

  ngOnInit() {
    this.getProduto();
  }

  getProduto(): void {
    this.produtoService.getProdutos().pipe(
      take(1))
      .subscribe((res) => (this.produtos = res));
  }

  get posuiVeiculos() {
    const posuiVeiculos = !!(this.produtos || []).filter(item => item.descricao === 'VEÍCULOS')[0];
    return posuiVeiculos;
  }

  get posuiCreditoPessoal() {
    const posuiCreditoPessoal = !!(this.produtos || []).filter(item => item.descricao === 'CRÉDITO PESSOAL')[0];
    return posuiCreditoPessoal;
  }

  get posuiCdcLoja() {
    const possuiCdcLoja = !!(this.produtos || []).filter(item => item.descricao === 'CDC LOJA')[0];
    return possuiCdcLoja;
  }

  get posuiCdcPremium() {
    const possuiCdcPremium = !!(this.produtos || []).filter(item => item.descricao === 'CDC PREMIUM')[0];
    return possuiCdcPremium;
  }

  onClickSelectAgente() {
    this.selecionarAgente.selecionar();
    this.selecionarAgente.onSelectAgente.pipe(
      take(1))
      .subscribe((result: SelectAgenteResult) => {
        if (result.status === SelectAgenteResultStatus.SUCCESS) {
          this.sharedService.notifyAgenteChange();
        } else if (result.status === SelectAgenteResultStatus.ERROR) {
          this.modalMessage.show();
        }
      });
  }
}
