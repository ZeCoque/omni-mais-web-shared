export class FichaListaFiltro {
  nomeLoja: string;
  nomeCpf: string;
  numero: string;
  status: string;
  data: string;
  sos: string;
}
