import { SeguroSimulacao } from './seguro-simulacao';

export class GrupoSeguro {
  id: number;
  descricao: string;
  seguros: SeguroSimulacao[];
}
