export class LojistaSimulacao {

  private id: number;

  constructor(obj: { id: number; }) {
    this.id = obj.id;
  }

  getId(): number {
    return this.id;
  }
}
