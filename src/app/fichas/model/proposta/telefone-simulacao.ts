export class TelefoneSimulacao {
  private ddd: string;
  private numero: string;

  constructor(obj) {
    this.ddd = obj.ddd;
    this.numero = obj.numero;
  }

  getDdd(): string {
    return this.ddd;
  }

  getNumero(): string {
    return this.numero;
  }
}
