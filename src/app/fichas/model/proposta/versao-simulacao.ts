export class VersaoSimulacao {
  private id: string;
  private descricao: string;

  constructor(obj: { id: string; descricao: string; }) {
    this.id = obj.id;
    this.descricao = obj.descricao || '';
  }

  getId(): string {
    return this.id;
  }

  getDescricao(): string {
    return this.descricao;
  }
}
