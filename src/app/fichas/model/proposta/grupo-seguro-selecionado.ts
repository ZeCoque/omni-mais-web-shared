export class GrupoSeguroSelecionado {
  public seguroId: number;
  public grupoId: number;

  constructor(seguroId: number, grupoId: number) {
    this.seguroId = seguroId;
    this.grupoId = grupoId;
  }
}
