export class GrupoAssistenciaSelecionado {
  public assistenciaId: number;
  public grupoId: number;

  constructor(assistenciaId: number, grupoId: number) {
    this.assistenciaId = assistenciaId;
    this.grupoId = grupoId;
  }
}
