import { AssistenciaSimulacao } from './assistencia-simulacao';

export class GrupoAssistencia {
  id: number;
  descricao: string;
  assistencias: AssistenciaSimulacao[];
}
