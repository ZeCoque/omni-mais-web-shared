import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichaService } from '../fichas.service';
import { FormsModule } from '@angular/forms';
import { ModalsModule } from 'src/app/modals/modals.module';
import { FichaCardComponent } from './ficha-card.component';
import { BellNotificationModule } from 'src/app/bell-notification/bell-notification.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  declarations: [
    FichaCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModalsModule,
    BellNotificationModule,
    PipesModule
  ],
  providers: [
    FichaService
  ],
  exports: [
    FichaCardComponent
  ]
})
export class FichaCardModule { }
