import { FichaListaFiltro } from '../../model/ficha-lista-filtro';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { fichasFilter, calcularQuantidadeSOS } from 'src/app/utils/filter';
@Component({
  selector: 'app-ficha-em-analise',
  templateUrl: './ficha-em-analise.component.html',
  styleUrls: ['./ficha-em-analise.component.css']
})
export class FichaEmAnaliseComponent implements OnInit {

  @Input() propostas = [];
  @Input() filtro: FichaListaFiltro;
  @Output() quantidadeSos = new EventEmitter();
  public fichasFiltradas: any;

  constructor() { }
  filter() {
    this.fichasFiltradas = fichasFilter(this.filtro, this.propostas);
    this.quantidadeSos.emit(calcularQuantidadeSOS(this.fichasFiltradas));
    return this.fichasFiltradas;
  }
  ngOnInit() {
  }
}
