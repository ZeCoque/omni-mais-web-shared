import { FichaListaFiltro } from '../../model/ficha-lista-filtro';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { fichasFilter, calcularQuantidadeSOS } from 'src/app/utils/filter';
@Component({
  selector: 'app-ficha-aprovada',
  templateUrl: './ficha-aprovada.component.html',
  styleUrls: ['./ficha-aprovada.component.css']
})
export class FichaAprovadaComponent implements OnInit {

  @Input() propostas = [];
  @Input() filtro: FichaListaFiltro;
  @Output() quantidadeSos =  new EventEmitter();
  public fichasFiltradas: any;
  constructor() { }

  filter() {
    this.fichasFiltradas = fichasFilter(this.filtro, this.propostas);
    this.quantidadeSos.emit(calcularQuantidadeSOS(this.fichasFiltradas));
    return this.fichasFiltradas;
  }

  ngOnInit() {
  }
}
