import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificacaoAbaComponent } from './notificacao-aba/notificacao-aba.component';
import { FichaAprovadaComponent } from './ficha-aprovada/ficha-aprovada.component';
import { FichaContratoCanceladoComponent } from './ficha-contrato-cancelado/ficha-contrato-cancelado.component';
import { FichaEmAnaliseComponent } from './ficha-em-analise/ficha-em-analise.component';
import { FichaEmFechamentoComponent } from './ficha-em-fechamento/ficha-em-fechamento.component';
import { FichaRecusadaComponent } from './ficha-recusada/ficha-recusada.component';
import { ListaFichaAprovadaComponent } from './lista-ficha-aprovada/lista-ficha-aprovada.component';
import { ListaFichaContratoCanceladoComponent } from './lista-ficha-contrato-cancelado/lista-ficha-contrato-cancelado.component';
import { ListaFichaRecusadaComponent } from './lista-ficha-recusada/lista-ficha-recusada.component';
import { ListaFichaEmAnaliseComponent } from './lista-ficha-em-analise/lista-ficha-em-analise.component';
import { ListaFichaEmFechamentoComponent } from './lista-ficha-em-fechamento/lista-ficha-em-fechamento.component';
import { FichaService } from '../fichas.service';
import { FormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'src/app/breadcrumb/breadcrumb.module';
import { SelectProdutoModule } from 'src/app/select-produto/select-produto.module';
import { FichaListaComponent } from './ficha-lista.component';
import { ModalsModule } from 'src/app/modals/modals.module';
import { LoaderModule } from 'src/app/loaders/loaders.module';
import { FichaCardModule } from '../ficha-card/ficha-card.module';
import { FichaTableModule } from '../ficha-table/ficha-table.module';
import { FichaListaService } from './ficha-lista.service';
import { TipoVisualizacaoService } from 'src/app/services/tipo-visualizacao.service';

@NgModule({
  declarations: [
    NotificacaoAbaComponent,
    FichaAprovadaComponent,
    FichaContratoCanceladoComponent,
    FichaEmFechamentoComponent,
    FichaEmAnaliseComponent,
    FichaRecusadaComponent,
    ListaFichaAprovadaComponent,
    ListaFichaContratoCanceladoComponent,
    ListaFichaEmAnaliseComponent,
    ListaFichaEmFechamentoComponent,
    ListaFichaRecusadaComponent,
    FichaListaComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    BreadcrumbModule,
    SelectProdutoModule,
    ModalsModule,
    LoaderModule,
    FichaCardModule,
    FichaTableModule
  ],
  providers: [
    FichaService,
    FichaListaService,
    TipoVisualizacaoService
  ]
})
export class FichasListaModule { }
