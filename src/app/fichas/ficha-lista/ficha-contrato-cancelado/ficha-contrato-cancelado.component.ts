import { FichaListaFiltro } from '../../model/ficha-lista-filtro';
import { Component, OnInit, Input } from '@angular/core';
import { fichasFilter } from 'src/app/utils/filter';

@Component({
  selector: 'app-ficha-contrato-cancelado',
  templateUrl: './ficha-contrato-cancelado.component.html',
  styleUrls: ['./ficha-contrato-cancelado.component.css']
})
export class FichaContratoCanceladoComponent implements OnInit {

  @Input() propostas = [];
  @Input() filtro: FichaListaFiltro;

  constructor() { }

  filter() {
    return fichasFilter(this.filtro, this.propostas);
  }

  ngOnInit() {
  }

}
