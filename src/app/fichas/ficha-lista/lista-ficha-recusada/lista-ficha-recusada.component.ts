import { Component, Input, OnInit } from '@angular/core';
import { FichaListaFiltro } from '../../model/ficha-lista-filtro';

@Component({
  selector: 'app-lista-ficha-recusada',
  templateUrl: './lista-ficha-recusada.component.html',
  styleUrls: ['./lista-ficha-recusada.component.css']
})
export class ListaFichaRecusadaComponent implements OnInit {
  @Input() propostas = [];
  @Input() filtro: FichaListaFiltro;

  constructor() { }

  ngOnInit() {
  }

}
