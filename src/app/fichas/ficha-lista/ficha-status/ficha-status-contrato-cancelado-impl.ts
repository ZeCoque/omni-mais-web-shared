import { StatusModel } from './../../../models/statusModel';
import { FichaStatus } from './ficha-status-interface';
export class StatusContratoCancelado implements FichaStatus{
  status:StatusModel[] = [];

  constructor() {
    this.status.push(new StatusModel('Contrato Cancelado', '19'));
    this.status.push(new StatusModel('Todos', '-1'));

  }

  getStatus() {
    return this.status;
  }
}
