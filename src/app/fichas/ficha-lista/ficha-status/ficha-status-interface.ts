import { StatusModel } from './../../../models/statusModel';
import { Observable } from 'rxjs';
export interface FichaStatus{

  status: StatusModel [];

  getStatus(): StatusModel [];

}
