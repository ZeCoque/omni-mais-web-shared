import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-notificacao-aba',
  templateUrl: './notificacao-aba.component.html',
  styleUrls: ['./notificacao-aba.component.css']
})
export class NotificacaoAbaComponent implements OnInit {

  @Input() quantidade = 0;

  constructor() { }

  ngOnInit() {
  }

}
