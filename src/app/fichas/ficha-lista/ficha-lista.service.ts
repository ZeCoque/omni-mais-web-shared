import { Observable ,  Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class FichaListaService {

  private reloadFichaListaSubject: Subject<any> = new Subject();
  reloadFichaListaObservable: Observable<any> = this.reloadFichaListaSubject.asObservable();

  constructor() {}

  reloadFichaLista() {
    this.reloadFichaListaSubject.next();
  }
}
