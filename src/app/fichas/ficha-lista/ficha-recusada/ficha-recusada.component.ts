import { FichaListaFiltro } from '../../model/ficha-lista-filtro';
import { Component, OnInit, Input } from '@angular/core';
import { fichasFilter } from 'src/app/utils/filter';

@Component({
  selector: 'app-ficha-recusada',
  templateUrl: './ficha-recusada.component.html',
  styleUrls: ['./ficha-recusada.component.css']
})
export class FichaRecusadaComponent implements OnInit {

  @Input() propostas = [];
  @Input() filtro: FichaListaFiltro;

  constructor() { }

  filter() {
    return fichasFilter(this.filtro, this.propostas);
  }

  ngOnInit() {
  }

}
