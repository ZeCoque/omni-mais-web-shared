
import { take, takeWhile, finalize } from 'rxjs/operators';
import { AfterViewChecked, ChangeDetectorRef, Component, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FichaService } from '../fichas.service';
import { LoginStorage } from './../../login/login.storage';
import { MenuSharedService } from './../../menu/menu-shared.service';
import { FichaListaService } from './ficha-lista.service';
import { StatusEmAnalise } from './ficha-status/ficha-status-analise-impl';
import { StatusAprovada } from './ficha-status/ficha-status-aprovada-impl';
import { StatusFechamento } from './ficha-status/ficha-status-fechamento-impl';
import { FichaStatus } from './ficha-status/ficha-status-interface';
import { StatusRecusada } from './ficha-status/ficha-status-recusada-impl';
import { FichaListaFiltro } from '../model/ficha-lista-filtro';
import { StatusContratoCancelado } from './ficha-status/ficha-status-contrato-cancelado-impl';
import { ModalMessageComponent } from 'src/app/modals/modal-message/modal-message.component';
import { SelectProdutoComponent } from 'src/app/select-produto/select-produto.component';
import { StatusModel } from 'src/app/models/statusModel';
import { TipoVisualizacaoService } from 'src/app/services/tipo-visualizacao.service';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha-lista.component.html',
  styleUrls: ['./ficha-lista.component.css']
})
export class FichaListaComponent
  implements OnInit, OnDestroy, AfterViewChecked, OnChanges {

  @ViewChild('modalMessage', { static: true })
  modalMessage: ModalMessageComponent;

  @ViewChild('produtos', { static: true })
  produtosComponent: SelectProdutoComponent;

  filtro: FichaListaFiltro = new FichaListaFiltro();

  propostasEmAnalise = [];
  quantSosPropostasEmAnalise = 0;
  propostasAprovada = [];
  quantSosPropostasAprovadas = 0;
  propostasEmFechamento = [];
  quantSosPropostasEmFechamento = 0;
  propostasRecusada = [];
  propostasContratoCancelado = [];

  propostasEmAnaliseFiltro = [];
  quantSosPropostasEmAnaliseFiltro = 0;

  isEmAnalise: boolean;
  isPesquisandoEmAnalise = false;

  isEmFechamento: boolean;
  isPesquisandoEmFechamento = false;

  isAprovada: boolean;
  isPesquisandoAprovada = false;

  isRecusada: boolean;
  isPesquisandoisRecusada = false;

  isContratoCancelado: boolean;
  isPesquisandoisContratoCancelado = false;

  isEmAnaliseFiltro: boolean;
  isPesquisandoEmAnaliseFiltro = false;

  isFilters = false;

  disableTabs = false;

  status: StatusModel[] = [];
  interval: any;
  isAlive = true;

  showLoader: boolean = false;

  isCardView: boolean;
  produtosFiltro: any;

  constructor(
    private ref: ChangeDetectorRef,
    private fichaService: FichaService,
    private router: Router,
    private loginStorage: LoginStorage,
    private menuSharedService: MenuSharedService,
    private FichaListaService: FichaListaService,
    private tipoVisualizacaoService: TipoVisualizacaoService
  ) { }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit() {
    debugger
    this.onClickEmAnalise();
    this.isCardView = this.loginStorage.usuario.visualizacaoProposta === 'C';
  }

  ngOnDestroy() {
    clearInterval(this.interval);
    this.isAlive = false;
  }

  ngOnChanges() {
  }

  private initObservableAgenteChange() {
    this.menuSharedService.onChangeAgente.pipe(
      takeWhile(() => this.isAlive))
      .subscribe(() => this.loadFichas());
  }

  private initObservableReloadFichas() {
    this.FichaListaService.reloadFichaListaObservable.pipe(
      takeWhile(() => this.isAlive))
      .subscribe(() => this.loadFichas());
  }

  refreshFichas() {
    if (
      !this.isPesquisandoEmAnalise &&
      !this.isPesquisandoAprovada &&
      !this.isPesquisandoEmFechamento &&
      !this.isPesquisandoisRecusada &&
      !this.isPesquisandoisContratoCancelado
    ) {
      this.loadFichas();
    }
  }

  onChangeFiltroProdutos(produtosFiltro: any[]) {
    this.produtosFiltro = produtosFiltro.map(
      (      ele: { descricaoNormalized: any; checked: any; }) => {
        return {
          descricao: ele.descricaoNormalized,
          checked: ele.checked
        };
      }
    );

    this.createDataFilterProdutos(produtosFiltro);
    this.loadFichas();
    this.initObservableAgenteChange();
    this.initObservableReloadFichas();
  }

  createDataFilterProdutos(idFase: any) {
    return {
      idFase,
      produtos: this.produtosFiltro,
    };
  }

  private loadFichas() {
    this.initVariablesControls();

    this.fichaService
      .findAllEmAnalise(this.createDataFilterProdutos(1)).pipe(
        finalize(() => (this.isPesquisandoEmAnalise = false)))
      .subscribe(
        (res: any) => {
          this.propostasEmAnalise = this.handlePropostas(res);
        },
        () => (this.propostasEmAnalise = [])
      );

    this.fichaService
      .findAllAprovada(this.createDataFilterProdutos(2)).pipe(
        finalize(() => (this.isPesquisandoAprovada = false)))
      .subscribe(
        (res: any) => {
          this.propostasAprovada = this.handlePropostas(res);
        },
        () => this.propostasAprovada
      );

    this.fichaService
      .findAllEmFechamento(this.createDataFilterProdutos(3)).pipe(
        finalize(() => (this.isPesquisandoEmFechamento = false)))
      .subscribe(
        (res: any) => {
          this.propostasEmFechamento = this.handlePropostas(res);
        },
        () => (this.propostasEmFechamento = [])
      );

    this.fichaService
      .findAllRecusada(this.createDataFilterProdutos(4)).pipe(
        finalize(() => (this.isPesquisandoisRecusada = false)))
      .subscribe(
        (res: any) => (this.propostasRecusada = this.handlePropostas(res)),
        () => (this.propostasRecusada = [])
      );

    if (this.showContratoCancelado) {
      this.fichaService
        .findAllContratoCancelado(this.createDataFilterProdutos(5)).pipe(
          finalize(() => (this.isPesquisandoisRecusada = false)))
        .subscribe(
          (res: any) => (this.propostasContratoCancelado = this.handlePropostas(res)),
          () => (this.propostasContratoCancelado = [])
        );
    }
  }

  private initVariablesControls() {
    this.quantSosPropostasEmAnalise = 0;
    this.quantSosPropostasAprovadas = 0;
    this.quantSosPropostasEmFechamento = 0;
    this.isPesquisandoEmAnalise = true;
    this.isPesquisandoAprovada = true;
    this.isPesquisandoEmFechamento = true;
    this.isPesquisandoisRecusada = true;
  }

  onChangeView(status: boolean) {
    if (status !== this.isCardView) {
      this.isCardView = status;
      this.saveTipoVisualizacao(this.isCardView);
    }
  }

  private saveTipoVisualizacao(isCardView: boolean): void {
    this.showLoader = true;

    this.tipoVisualizacaoService
      .setVisualizacao({
        idAgente: this.loginStorage.usuario.id,
        tipoVisualizacao: isCardView ? 'C' : 'L'
      }).pipe(
        take(1),
        finalize(() => this.showLoader = false))
      .subscribe();
  }

  onClickEmAnalise(): void {
    this.hideAllTabs();
    this.isEmAnalise = true;
    this.filtro.data = '30';
    this.setStatus(new StatusEmAnalise());
    this.filtro.status = '-1';
  }

  onClickEmFechamento(): void {
    this.hideAllTabs();
    this.isEmFechamento = true;
    this.filtro.data = '30';
    this.setStatus(new StatusFechamento());
    this.filtro.status = '-1';
  }

  onClickEmAprovada(): void {
    this.hideAllTabs();
    this.isAprovada = true;
    this.filtro.data = '30';
    this.setStatus(new StatusAprovada());
    this.filtro.status = '-1';
  }

  onClickEmRecusada(): void {
    this.hideAllTabs();
    this.isRecusada = true;
    this.filtro.data = '30';
    this.setStatus(new StatusRecusada());
    this.filtro.status = '-1';
  }

  onClickEmContratoCancelado(): void {
    this.hideAllTabs();
    this.isContratoCancelado = true;
    this.filtro.data = '30';
    this.setStatus(new StatusContratoCancelado);
    this.filtro.status = '-1';
  }

  private hideAllTabs() {
    this.isEmAnalise = false;
    this.isEmFechamento = false;
    this.isAprovada = false;
    this.isRecusada = false;
    this.isContratoCancelado = false;
  }

  private setStatus(status: FichaStatus): void {
    this.status = status.getStatus();
  }

  private handlePropostas(res: any) {
    return res && res.propostasFase && res.propostasFase[0]
      ? res.propostasFase[0].propostas
      : [];
  }

  quantidadeSosAnalise(data: number) {
    this.quantSosPropostasEmAnalise = data;
  }
  quantidadeSosAprovada(data: number) {
    this.quantSosPropostasAprovadas = data;
  }
  quantidadeSosFechamento(data: number) {
    this.quantSosPropostasEmFechamento = data;
  }


  addFicha() {
    if (this.loginStorage.usuario.excedeProducao === 'N') {
      this.router.navigate(['/nova']);
    } else {
      this.modalMessage.show(
        'Excedido o percentual de produção permitido para o mês.'
      );
    }
  }

  get showContratoCancelado() {
    return (this.produtosComponent.produtos || []).map((item: { descricao: any; }) => item.descricao).includes('CDC LOJA');
  }

}
