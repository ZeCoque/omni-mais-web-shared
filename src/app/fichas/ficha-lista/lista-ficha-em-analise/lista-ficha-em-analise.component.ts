import { Component, Input, OnInit } from '@angular/core';
import { FichaListaFiltro } from '../../model/ficha-lista-filtro';

@Component({
  selector: 'app-lista-ficha-em-analise',
  templateUrl: './lista-ficha-em-analise.component.html',
  styleUrls: ['./lista-ficha-em-analise.component.css']
})
export class ListaFichaEmAnaliseComponent implements OnInit {
  @Input() propostas = [];
  @Input() filtro: FichaListaFiltro;

  constructor() {}

  ngOnInit() { }
}
