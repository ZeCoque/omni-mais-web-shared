import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichaTableModule } from './ficha-table/ficha-table.module';
import { FichaService } from './fichas.service';
import { FichasListaModule } from './ficha-lista/ficha-lista.module';
import { FichaCardModule } from './ficha-card/ficha-card.module';
import { OmniVerificacaoSegurancaService } from '../omni-rest/omni-verificacao-seguranca.service';

@NgModule({
  imports: [
    CommonModule,
    FichaTableModule,
    FichasListaModule,
    FichaCardModule
  ],
  providers: [
    FichaService,
    OmniVerificacaoSegurancaService
  ]
})
export class FichasModule { }
