import { HeaderSort } from './header-sort';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-header-sort',
  templateUrl: './header-sort.component.html',
  styleUrls: ['./header-sort.component.css'],
})
export class HeaderSortComponent implements OnInit {

  @Input() description: string;
  @Input() column: string;
  @Input() current: BehaviorSubject<HeaderSort>;

  @Output() sort = new EventEmitter<HeaderSort>();

  direction = 'desc';

  constructor() { }

  ngOnInit() {
  }

  onClickDirection() {
    this.direction = this.direction === 'asc' ? 'desc' : 'asc';
    this.sort.emit(new HeaderSort(this.column, this.direction));
  }

}
