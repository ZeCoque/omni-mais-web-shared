import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichaTableComponent } from './ficha-table.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { HeaderSortComponent } from './header-sort/header-sort.component';
import { ModalsModule } from 'src/app/modals/modals.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FichaTableComponent,
    PaginatorComponent,
    HeaderSortComponent
  ],
  imports: [
    CommonModule,
    ModalsModule,
    PipesModule,
    FormsModule
  ],
  exports: [
    FichaTableComponent,
    PaginatorComponent,
    HeaderSortComponent
  ]
})
export class FichaTableModule { }
