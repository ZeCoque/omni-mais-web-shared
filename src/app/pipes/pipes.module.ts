import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SingleSpaService } from '../services/single-spa.service';
import { AssetUrlPipe } from './asset-url-pipe';
import { CpfPipe } from './cpf/cpf.pipe';
import { DateTimePipe } from './date/date-time.pipe';
import { TelefonePipe } from './telefone/telefone.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CpfPipe,
    DateTimePipe,
    TelefonePipe,
    AssetUrlPipe
  ],
  exports: [
    CpfPipe,
    DateTimePipe,
    TelefonePipe,
    AssetUrlPipe
  ],
  providers: [
    SingleSpaService
  ]
})
export class PipesModule { }
