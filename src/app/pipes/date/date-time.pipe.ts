import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'datetime'
})
export class DateTimePipe implements PipeTransform {

  transform(value: string): any {
    if (value) {
      return moment(value).format('DD/MM/YYYY - HH:mm');
    }
    return value;
  }
}
