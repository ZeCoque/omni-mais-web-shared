import { Pipe, PipeTransform } from '@angular/core';
import { SingleSpaService } from '../services/single-spa.service';

@Pipe({ name: 'assetUrl' })
export class AssetUrlPipe implements PipeTransform {
  constructor(
    private service: SingleSpaService,
  ) { }

  transform(value: string): string {
    return this.service.assetUrl(value);
  }
}
