import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BellNotificationComponent } from './bell-notification.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    BellNotificationComponent
  ],
  exports: [
    BellNotificationComponent
  ]
})
export class BellNotificationModule { }
