import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bell-notification',
  templateUrl: './bell-notification.component.html',
  styleUrls: ['./bell-notification.component.css']
})
export class BellNotificationComponent implements OnInit {

  @Input() count: number;

  constructor() { }

  ngOnInit() {
  }

}
