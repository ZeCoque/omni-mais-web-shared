import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { OmniGeralRestService } from './omni-geral/omni-geral-rest.service';
import { OmniMaisRestService } from './omni-mais/omni-mais-rest.service';
import { OmniWsRestService } from './omni-ws/omni-ws-rest.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    OmniWsRestService,
    OmniGeralRestService,
    OmniMaisRestService,
  ]
})
export class OmniRestModule { }
