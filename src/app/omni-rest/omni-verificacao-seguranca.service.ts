import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginStorage } from '../login/login.storage';
import { OmniRestService } from './omni-rest.service';
import { LoaderService } from '../loaders/loader/loader.service';

@Injectable()
export class OmniVerificacaoSegurancaService extends OmniRestService {
  constructor(
    http: HttpClient,
    loaderService: LoaderService,
    loginStorage: LoginStorage,
    router: Router
  ) {
    super(http, loaderService, loginStorage, router);
  }

  getBaseUrl(): string {
    return environment.URL_OMNI_MAIS_API;
  }

  getHeaders() {
    return {
      'Content-Type': 'application/json'
    };
  }

}
