
import { throwError as observableThrowError, empty as observableEmpty,  Observable } from 'rxjs';

import { finalize, catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginStorage } from '../login/login.storage';
import { HttpStatus } from './http-status';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from '../loaders/loader/loader.service';

export abstract class OmniRestService {

  constructor(
    protected http: HttpClient,
    protected loaderService: LoaderService,
    protected loginStorage: LoginStorage,
    protected router: Router) { }

  abstract getBaseUrl(): string;

  abstract getHeaders(): { [header: string]: string };

  post<T>(url: string, body?: any): Observable<T> {
    if (body) {
      return this.intercept(
        this.http.post(
          this.getBaseUrl().concat(url),
          JSON.stringify(body),
          { headers: this.getHeaders(), responseType: 'text', observe: 'response' }
        )
      );
    }
    return this.intercept(this.http.post(this.getBaseUrl().concat(url), {}, { headers: this.getHeaders(), responseType: 'text', observe: 'response' }));
  }

  put<T>(url: string, body?: any): Observable<T> {
    if (body) {
      return this.intercept(
        this.http.put(
          this.getBaseUrl().concat(url),
          JSON.stringify(body),
          { headers: this.getHeaders(), responseType: 'text', observe: 'response' }
        )
      );
    }
    return this.intercept(this.http.put(this.getBaseUrl().concat(url), {}, { headers: this.getHeaders(), responseType: 'text', observe: 'response' }));
  }

  get(url: string, options?: any): Observable<any> {
    return this.intercept(this.http.get(this.getBaseUrl().concat(url), { ...options, headers: this.getHeaders(), responseType: 'text', observe: 'response' }));
  }

  delete(url: string, body?: any): Observable<any> {
    return this.intercept(this.http.request('delete', this.getBaseUrl().concat(url), {
      body,
      headers: this.getHeaders(),
      responseType: 'text', observe: 'response'
    }));
  }

  intercept(observable: Observable<any>): Observable<any> {
    this.loaderService.show();
    return observable.pipe(
      map(res => {
        if (res.body && !['SUCCESS', 'FAIL'].includes(res.body.replace(/\"/g, ''))) {
          return JSON.parse(res.body);
        }
        return res;
      }),
      catchError((error, source) => {
        if (error && error.status && error.status === HttpStatus.UNAUTHORIZED) {
          this.router.navigate(['/login']);
          return observableEmpty();
        }

        if (error.text && error.text()) {
          error.error = error.json();
        } else if (typeof error.error === 'string') {
          error.error = JSON.parse(error.error);
        }

        return observableThrowError(error);
      }),
      finalize(() => {
        this.loaderService.hide();
      })
    );
  }
}
