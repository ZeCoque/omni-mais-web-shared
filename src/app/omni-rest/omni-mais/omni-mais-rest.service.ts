import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginStorage } from '../../login/login.storage';
import { OmniRestService } from '../omni-rest.service';
import { environment } from '../../../environments/environment';
import { LoaderService } from 'src/app/loaders/loader/loader.service';

@Injectable()
export class OmniMaisRestService extends OmniRestService {

  constructor(http: HttpClient, loaderService: LoaderService, loginStorage: LoginStorage, router: Router) {
    super(http, loaderService, loginStorage, router);
  }

  getBaseUrl(): string {
    return environment.URL_OMNI_MAIS_API;
  }

  getHeaders() {
    return {
      'Content-Type': 'application/json',
      'omni-autenticacao': this.loginStorage.loginAndTokenAsJson
    };
  }
}
