import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { LoginStorage } from '../../login/login.storage';
import { OmniRestService } from '../omni-rest.service';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from 'src/app/loaders/loader/loader.service';

@Injectable()
export class OmniGeralRestService extends OmniRestService {

  constructor(http: HttpClient, loaderService: LoaderService, loginStorage: LoginStorage, router: Router) {
    super(http, loaderService, loginStorage, router);
  }

  getBaseUrl(): string {
    return environment.URL_OMNI_GERAL;
  }

  getHeaders() {
    return {
      'Content-Type': 'application/json',
      'omni-autenticacao': this.loginStorage.loginAndTokenAsJson
    };
  }
}
