export class HttpStatus {
  public static ACCEPTED = 202;
  public static BAD_REQUEST = 400;
  public static UNAUTHORIZED = 401;
  public static NOT_FOUND = 404;
  public static PRECONDITION_FAILED = 412;
  public static INTERNAL_SEVER_ERROR = 500;
  public static FORBIDDEN = 403;
}
