import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginStorage } from '../../login/login.storage';
import { OmniRestService } from '../omni-rest.service';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from 'src/app/loaders/loader/loader.service';
import { environment } from 'src/environments/environment';
import { assetUrl } from 'src/single-spa/asset-url';

@Injectable()
export class OmniWsRestService extends OmniRestService {
  constructor(
    http: HttpClient,
    loaderService: LoaderService,
    loginStorage: LoginStorage,
    router: Router,
  ) {
    super(http, loaderService, loginStorage, router);
  }

  getBaseUrl(): string {
    return assetUrl(`${environment.URL_OMNI_FACIL}omni-ws/api/`, false);
  }

  getHeaders() {
    return {
      'Content-Type': 'application/json'
    };
  }
}
