import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap';
import { ModalMessageComponent } from './modal-message/modal-message.component';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    ModalComponent,
    ModalMessageComponent,
  ],
  exports: [
    ModalComponent,
    ModalMessageComponent,
  ],
  entryComponents: [
    ConfirmModalComponent
  ]
})
export class ModalsModule { }
