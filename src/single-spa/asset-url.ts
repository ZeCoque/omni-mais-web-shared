// In single-spa, the assets need to be loaded from a dynamic location,
// instead of hard coded to `/assets`. We use webpack public path for this.
// See https://webpack.js.org/guides/public-path/#root

export function assetUrl(url: string, withAssets = true): string {
  // @ts-ignore
  const publicPath = withAssets ? __webpack_public_path__ : __webpack_public_path__.slice(0, -1);;
  const publicPathSuffix = publicPath.endsWith('/') ? '' : '/';
  const urlPrefix = url.startsWith('/') ? '' : '/';
  const assets = withAssets ? 'assets' : '';

  return `${publicPath}${withAssets ? publicPathSuffix : ''}${assets}${urlPrefix}${url}`;
}
